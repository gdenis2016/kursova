DROP DATABASE IF EXISTS eurovision;
CREATE DATABASE eurovision;
USE eurovision;

CREATE TABLE performers(
	id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(30),
    country VARCHAR(30),
    song VARCHAR(30),
    rating_position INT
);

INSERT INTO performers(name, country, song, rating_position) VALUES 
('Скрябін','Ukraine','Сам собі країна',1),
('Rammstein','Germany','Du Hast', 1),
('Ruslana','Ukraine','Wild Dances', 2),
('Avicii','Sweden','Wake Me Up', 1),
('One Republic','United Kingdom','Counting Stars', 1);
