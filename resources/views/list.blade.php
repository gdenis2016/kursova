<link rel="stylesheet" href="{{ asset('css/main.css') }}">

<h1>Інформаційна система Євробачення</h1>
<div class="nav">
    <a href="/performers">Адмінка</a>
    <a href="/login">Увійти</a>
    <a href="/register">Зареєструватись</a>
</div>

<div class="container">
    <table border="1">
        <tr>
            <td>ID</td>
            <td>ФІО виконавця</td>
            <td>Країна</td>
            <td>Назва пісні</td>
            <td>Рейтинг</td>
        </tr>
        @foreach($performers as $performer)
            <tr>
                <td>{{$performer->id}}</td>
                <td><a href="/performers/{{$performer->id}}">{{$performer->name}}</a></td>
                <td>{{$performer->country}}</td>
                <td>{{$performer->song}}</td>
                <td>{{$performer->rating_position}}</td>
            </tr>
        @endforeach
    </table>
</div>


