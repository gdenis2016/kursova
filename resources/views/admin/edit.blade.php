<link rel="stylesheet" href="{{ asset('css/main.css') }}">
<h1>Редагувати учасника</h1>

<div class="container">
    <form action="/performers/{{$performer->id}}" method="post">
        @csrf
        @method('PUT')
        <span>Псевдоним: <input type="text" name="name" placeholder="Псевдоним" value="{{$performer->name}}"></span> <br><br>
        <span>Країна: <input type="text" name="country" placeholder="Країна" value="{{$performer->country}}"></span> <br><br>
        <span>Назва пісні: <input type="text" name="song" placeholder="Назва пісні" value="{{$performer->song}}"></span> <br><br>
        <span>Отримане місце: <input type="text" name="rating_position" placeholder="отримане місце" value="{{$performer->rating_position}}"></span> <br><br>
        <input type="submit" value="Редагувати">
    </form>
</div>
