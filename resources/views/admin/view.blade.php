<link rel="stylesheet" href="{{ asset('css/main.css') }}">

<h1>Адмінка</h1>
<div class="nav">
    <a href="/">На головну</a>
    <a href="/performers/create">Додати замовлення</a>
    <form action="{{ route('logout') }}" method="POST" class="d-none">
        @csrf
        <input type="submit" value="Вийти з аккаунту">
    </form>
</div>

<div class="container">
    <form>
        <span>Пошук за назвою пісні: <input type="text" name="song" placeholder="Назва" value="{{$request->song}}"></span>

        <span>Сортувати:
        <select name="sort">
            <option value="">Не сортувати</option>
            @foreach(\App\Models\Performer::SORT_TYPES as $key => $type)
                <option value="{{$key}}" {{($request->sort == $key) ? 'selected' : ''}}>{{$type}}</option>
            @endforeach
        </select>
    </span>
        <span>Топ 3 переможців: <input type="checkbox" name="top3" {{($request->top3 == 'on') ? 'checked' : ''}}></span>
        <input type="submit" value="Відобразити">
    </form>
    <a href="/performers">Скинути фільтри</a><br><br>

    <table border="1">
        <tr>
            <td>ID</td>
            <td>ФІО виконавця</td>
            <td>Країна</td>
            <td>Назва пісні</td>
            <td>Рейтинг</td>
            <td colspan="2">Операції</td>
        </tr>
        @foreach($performers as $performer)
            <tr>
                <td>{{$performer->id}}</td>
                <td><a href="/performers/{{$performer->id}}">{{$performer->name}}</a></td>
                <td>{{$performer->country}}</td>
                <td>{{$performer->song}}</td>
                <td>{{$performer->rating_position}}</td>
                <td><a href="/performers/{{$performer->id}}/edit">Редагувати запис</a></td>
                <form action="/performers/{{$performer->id}}" method="post">
                    @csrf
                    @method('DELETE')
                    <td><input type="submit" style="width: 100%" value="Видалити місце"></td>
                </form>
            </tr>
        @endforeach
    </table>

</div>

