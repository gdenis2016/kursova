<link rel="stylesheet" href="{{ asset('css/main.css') }}">
<h1>Додати учасника</h1>

<div class="container">
    <form action="/performers" method="post">
        @csrf
        <span>Псевдоним: <input type="text" name="name" placeholder="Псевдоним"></span> <br><br>
        <span>Країна: <input type="text" name="country" placeholder="Країна"></span> <br><br>
        <span>Назва пісні: <input type="text" name="song" placeholder="Назва пісні"></span> <br><br>
        <span>Отримане місце: <input type="number" name="rating_position" placeholder="Отримане місце"></span> <br><br>
        <input type="submit" value="Додати">
    </form>
</div>

