<?php

namespace Tests\Feature;

use App\Models\Performer;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class FeatureTest extends TestCase
{
    private $performer;

    public function setUp(): void
    {
        parent::setUp();
        $this->performer = new Performer();
        $this->performer->name = 'Conchita Wurst';
        $this->performer->country = 'Austria';
        $this->performer->song = 'Rise Like a Phoenix';
        $this->performer->rating_position = '1';
        $this->performer->save();
    }

    public function test_Status()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
        $response->assertSee('Інформаційна система Євробачення');
    }

    public function test_Login()
    {
        $response = $this->
        followingRedirects()->
        call('POST', '/login', ['email' => 'g@g.g', 'password' => '12345678']);

        $response->assertSee('Адмінка');
    }

    public function test_SortArr()
    {
        $this->assertContains('за країною', Performer::SORT_TYPES);
    }

    public function test_CheckExists()
    {
        $performer = Performer::query()->where('name', '=', 'Conchita Wurst')->first();

        $this->assertEquals('Conchita Wurst', $performer->name);
        $this->assertEquals(1, $performer->rating_position);
    }

    public function tearDown(): void
    {
        parent::tearDown();
        $this->performer->delete();
    }
}
