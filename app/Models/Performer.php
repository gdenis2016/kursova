<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Performer extends Model
{
    use HasFactory;

    public const SORT_TYPES = [
        '1' => 'за країною',
        '2' => 'за місцем',
        '3' => 'за назвою пісні',
    ];

    protected $table = 'performers';
    public $timestamps = false;
    public $incrementing = true;

    protected $fillable = [
        'name',
        'country',
        'song',
        'rating_position',
    ];
}
