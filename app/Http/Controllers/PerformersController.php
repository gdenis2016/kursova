<?php

namespace App\Http\Controllers;

use App\Models\Performer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class PerformersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['main']);
    }

    public function main(){
        return view('list', ['performers' => Performer::all()]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        $query = Performer::query();

        if(filled($request->song))
            $query->where('song', 'LIKE', '%'.$request->song.'%');

        if(filled($request->top3)){
            $query->where('rating_position', '=', '1')->limit(3);
        }

        switch ($request->sort){
            case '1':
                $query->orderBy('country');
                break;
            case '2':
                $query->orderBy('rating_position');
                break;
            case '3':
                $query->orderBy('song');
                break;
            default:
                break;
        }

        return view('admin.view', ['request' => $request, 'performers' => $query->get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $performer = new Performer();

        $performer->name = $request->name;
        $performer->country = $request->country;
        $performer->song = $request->song;
        $performer->rating_position = $request->rating_position;

        $performer->save();

        return Redirect::to('/performers');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Performer  $performer
     * @return \Illuminate\Http\Response
     */
    public function show(Performer $performer)
    {
        return view('performer', ['performer' => $performer]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Performer  $performer
     * @return \Illuminate\Http\Response
     */
    public function edit(Performer $performer)
    {
        return view('admin.edit', ['performer' => $performer]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Performer  $performer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Performer $performer)
    {
        $performer->name = $request->name;
        $performer->country = $request->country;
        $performer->song = $request->song;
        $performer->rating_position = $request->rating_position;

        $performer->save();

        return Redirect::to('/performers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Performer  $performer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Performer $performer)
    {
        $performer->delete();
        return Redirect::to('/performers');
    }
}
