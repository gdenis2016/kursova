<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PerformersController;


Route::get('/', [PerformersController::class, 'main']);

Auth::routes();
Route::resource('performers', PerformersController::class);
